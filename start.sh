#!/bin/bash

set -eu -o pipefail

mkdir -p /run/gitea/tmp/uploads /run/sshd /run/gitea/sessions

setup_ldap_source() {
    set -eu

    # Get the existing LDAP source status. This allows the user to disable LDAP
    # Note that this method is deprecated since this app now supports optionalSso
    ldap_status=$(mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h mysql --database="${CLOUDRON_MYSQL_DATABASE}" -N -B -e "select is_actived from login_source WHERE name='cloudron';")
    [[ -z "${ldap_status}" ]] && ldap_status="1"

    now=$(date +%s)

    if mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h mysql --database="${CLOUDRON_MYSQL_DATABASE}" \
        -e "REPLACE INTO login_source (id, type, name, is_actived, cfg, created_unix, updated_unix) VALUES (1,2,'cloudron',${ldap_status},'{\"Name\":\"cloudron\",\"Host\":\"${CLOUDRON_LDAP_SERVER}\",\"Port\":${CLOUDRON_LDAP_PORT},\"UseSSL\":false,\"SkipVerify\":true,\"BindDN\":\"${CLOUDRON_LDAP_BIND_DN}\",\"BindPassword\":\"${CLOUDRON_LDAP_BIND_PASSWORD}\",\"UserBase\":\"${CLOUDRON_LDAP_USERS_BASE_DN}\",\"AttributeUsername\":\"username\",\"AttributeName\":\"displayname\",\"AttributeSurname\":\"\",\"AttributeMail\":\"mail\",\"Filter\":\"(\\\\u007C(mail=%[1]s)(username=%[1]s))\"}','${now}','${now}');"; then
        echo "==> LDAP Authentication was setup with activation status ${ldap_status}"
    else
        echo "==> Failed to setup LDAP authentication"
        exit 1
    fi
}

setup_root_user() {
    set -eu

    if sudo -H -u git /home/git/gitea/gitea admin user create --name root --password changeme --email test@cloudron.io --admin -c /run/gitea/app.ini; then
        echo "==> root user added"
    else
        echo "==> Failed to add root user"
        exit 1
    fi
}

setup_auth() {
    set -eu

    # Wait for gitea to finish db setup, before we do any db operations
    while ! curl --fail http://localhost:3000/explore; do
        echo "==> Waiting for gitea to come up"
        sleep 1
    done

    echo "==> Gitea is up, setting up auth"

    if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        setup_ldap_source
    fi

    user_count=$(mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h mysql --database="${CLOUDRON_MYSQL_DATABASE}" -N -B -e "SELECT count(*) FROM user;")
    # be careful, not to create root user for existing LDAP based installs
    if [[ "${user_count}" == "0" ]]; then
        echo "==> Setting up root user for first run"
        setup_root_user
    fi
}

# SSH_PORT can be unset to disable SSH
disable_ssh="false"
if [[ -z "${SSH_PORT:-}" ]]; then
    echo "SSH disabled"
    SSH_PORT=29418 # arbitrary port to keep sshd happy
    disable_ssh="true"
fi

if [[ ! -f "/app/data/sshd/ssh_host_ed25519_key" ]]; then
    echo "Generating ssh host keys"
    mkdir -p /app/data/sshd
    ssh-keygen -qt rsa -N '' -f /app/data/sshd/ssh_host_rsa_key
    ssh-keygen -qt dsa -N '' -f /app/data/sshd/ssh_host_dsa_key
    ssh-keygen -qt ecdsa -N '' -f /app/data/sshd/ssh_host_ecdsa_key
    ssh-keygen -qt ed25519 -N '' -f /app/data/sshd/ssh_host_ed25519_key
else
    echo "Reusing existing host keys"
fi

chmod 0600 /app/data/sshd/*_key
chmod 0644 /app/data/sshd/*.pub

sed -e "s/^Port .*/Port ${SSH_PORT}/" /etc/ssh/sshd_config > /run/gitea/sshd_config

if [[ ! -f /app/data/app.ini ]]; then
    echo -e "; Add customizations here - https://docs.gitea.io/en-us/config-cheat-sheet/" > /app/data/app.ini

    echo "==> Generating new SECRET_KEY"
    crudini --set "/app/data/app.ini" security SECRET_KEY $(pwgen -1 -s)
fi

# merge user config file
cp /home/git/app.ini.template "/run/gitea/app.ini"
crudini --merge "/run/gitea/app.ini" < "/app/data/app.ini"

# override important values
crudini --set "/run/gitea/app.ini" database DB_TYPE mysql
crudini --set "/run/gitea/app.ini" database HOST "${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}"
crudini --set "/run/gitea/app.ini" database NAME "${CLOUDRON_MYSQL_DATABASE}"
crudini --set "/run/gitea/app.ini" database USER "${CLOUDRON_MYSQL_USERNAME}"
crudini --set "/run/gitea/app.ini" database PASSWD "${CLOUDRON_MYSQL_PASSWORD}"
crudini --set "/run/gitea/app.ini" database SSL_MODE "disable"
crudini --set "/run/gitea/app.ini" server PROTOCOL "http"
crudini --set "/run/gitea/app.ini" server DOMAIN "${CLOUDRON_APP_DOMAIN}"
crudini --set "/run/gitea/app.ini" server ROOT_URL "https://%(DOMAIN)s/"
crudini --set "/run/gitea/app.ini" server HTTP_ADDR ""
crudini --set "/run/gitea/app.ini" server HTTP_PORT "3000"
crudini --set "/run/gitea/app.ini" server DISABLE_SSH "${disable_ssh}"
crudini --set "/run/gitea/app.ini" server SSH_PORT "${SSH_PORT}"
crudini --set "/run/gitea/app.ini" server APP_DATA_PATH "/app/data/appdata"
crudini --set "/run/gitea/app.ini" repository ROOT "/app/data/repository"
crudini --set "/run/gitea/app.ini" repository.upload TEMP_PATH "/run/gitea/tmp/uploads"
crudini --set "/run/gitea/app.ini" mailer HOST "${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTPS_PORT}"
crudini --set "/run/gitea/app.ini" mailer USER "${CLOUDRON_MAIL_SMTP_USERNAME}"
crudini --set "/run/gitea/app.ini" mailer PASSWD "${CLOUDRON_MAIL_SMTP_PASSWORD}"
crudini --set "/run/gitea/app.ini" mailer FROM "${CLOUDRON_MAIL_FROM}"
crudini --set "/run/gitea/app.ini" mailer SKIP_VERIFY "true"
crudini --set "/run/gitea/app.ini" security INSTALL_LOCK "true"
crudini --set "/run/gitea/app.ini" log MODE "console"
crudini --set "/run/gitea/app.ini" log ROOT_PATH "/run/gitea"
crudini --set "/run/gitea/app.ini" indexer ISSUE_INDEXER_PATH "/app/data/appdata/indexers/issues.bleve"

echo "==> Creating dirs and changing permissions"
mkdir -p /app/data/repository /app/data/ssh /app/data/custom
chown -R git:git /app/data /run/gitea

# this expects app.ini to be available
( setup_auth ) &

exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Gitea

