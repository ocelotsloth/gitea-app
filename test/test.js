#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD || !process.env.EMAIL) {
    console.log('USERNAME, PASSWORD and EMAIL env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var LOCATION = 'test';
    var SSH_PORT = 29420;

    var app;
    var browser;

    var repodir = '/tmp/testrepo';
    var reponame = 'testrepo';

    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var email = process.env.EMAIL;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
        rimraf.sync(repodir);
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function setAvatar() {
        await browser.get('https://' + app.fqdn + '/user/settings');

        var button = await browser.findElement(By.xpath('//label[contains(text(), "Use Custom Avatar")]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.findElement(By.xpath('//label[contains(text(), "Use Custom Avatar")]')).click();
        await browser.findElement(By.xpath('//input[@type="file" and @name="avatar"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        await browser.findElement(By.xpath('//button[contains(text(), "Update Avatar")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(),"Your avatar has been updated.")]')), TIMEOUT);
    }

    async function checkAvatar() {
        await browser.get(`https://${app.fqdn}/${username}`);

        var avatarSrc = await browser.findElement(By.xpath('//a[@id="profile-avatar"]/img')).getAttribute('src');

        var avatar = await superagent.get(avatarSrc);
        expect(avatar.statusCode).to.equal(200);
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn + '/user/login');

        await browser.findElement(By.id('user_name')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//form[@action="/user/login"]//button')).click();
        await browser.wait(until.elementLocated(By.linkText('Dashboard')), TIMEOUT);
    }

    async function adminLogin() {
        await login('root', 'changeme');
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);

        await browser.findElement(By.xpath('//img[contains(@class, "avatar")]')).click();
        await sleep(2000);
        await browser.findElement(By.xpath('//a[@data-url="/user/logout"]')).click();
        await sleep(2000);
    }

    async function addPublicKey() {
        var publicKey = fs.readFileSync(__dirname + '/id_rsa.pub', 'utf8');

        const sshPage = 'https://' + app.fqdn + '/user/settings/keys';

        await browser.get(sshPage);

        await browser.findElement(By.xpath('//div[text()="Add Key"]')).click();
        await browser.findElement(By.id('ssh-key-title')).sendKeys('testkey');
        await browser.findElement(By.id('ssh-key-content')).sendKeys(publicKey.trim()); // #3480
        var button = browser.findElement(By.xpath('//button[contains(text(), "Add Key")]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.findElement(By.xpath('//button[contains(text(), "Add Key")]')).click();

        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(), "has been added.")]')), TIMEOUT);
    }

    async function createRepo() {
        var getRepoPage = await browser.get('https://' + app.fqdn + '/repo/create');

        await browser.findElement(By.id('repo_name')).sendKeys(reponame);
        var button = browser.findElement(By.xpath('//button[contains(text(), "Create Repository")]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.findElement(By.id('auto-init')).click();
        await browser.findElement(By.xpath('//button[contains(text(), "Create Repository")]')).click();

        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === 'https://' + app.fqdn + '/' + username + '/' + reponame;
            });
        }, TIMEOUT);
    }

    async function checkCloneUrl() {
        await browser.get('https://' + app.fqdn + '/' + username + '/' + reponame);
        await browser.findElement(By.id('repo-clone-ssh')).click();

        var cloneUrl = await browser.findElement(By.id('repo-clone-url')).getAttribute('value');
        expect(cloneUrl).to.be(`ssh://git@${app.fqdn}:${SSH_PORT}/${username}/${reponame}.git`);
    }

    function cloneRepo() {
        rimraf.sync(repodir);
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync(`git clone ssh://git@${app.fqdn}:${SSH_PORT}/${username}/${reponame}.git ${repodir}`, { env: env });
    }

    function pushFile() {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync(`touch newfile && git add newfile && git commit -a -mx && git push ssh://git@${app.fqdn}:${SSH_PORT}/${username}/${reponame} master`,
                 { env: env, cwd: repodir });
        rimraf.sync('/tmp/testrepo');
    }

    function fileExists() {
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
    }

    async function sendMail() {
        await browser.get(`https://${app.fqdn}/admin/config`);

        var button = await browser.findElement(By.xpath('//button[@id="test-mail-btn"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys('test@cloudron.io');
        await browser.findElement(By.xpath('//button[@id="test-mail-btn"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(),"A testing email has been sent to \'test@cloudron.io\'")]')), TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION} -p SSH_PORT=${SSH_PORT}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', adminLogin);
    it('can send mail', sendMail);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can set avatar', setAvatar);
    it('can get avatar', checkAvatar);

    it('can add public key', addPublicKey);

    it('can create repo', createRepo);

    it('displays correct clone url', checkCloneUrl);

    it('can clone the url', cloneRepo);

    it('can add and push a file', pushFile);

    it('can restart app', function () { execSync('cloudron restart  --app ' + app.id); });

    xit('can login', login.bind(null, username, password)); // no need to relogin since session persists
    it('displays correct clone url', checkCloneUrl);
    it('can clone the url', cloneRepo);
    it('file exists in repo', fileExists);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () { execSync('cloudron restore --app ' + app.id, EXEC_ARGS); });

    it('can login', login.bind(null, username, password));
    it('can get avatar', checkAvatar);
    it('can clone the url', cloneRepo);
    it('file exists in repo', function () { expect(fs.existsSync(repodir + '/newfile')).to.be(true); });

    it('move to different location', async function () {
        //browser.manage().deleteAllCookies(); // commented because of error "'Network.deleteCookie' wasn't found"
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');

        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can login', login.bind(null, username, password));
    it('can get avatar', checkAvatar);
    it('displays correct clone url', checkCloneUrl);
    it('can clone the url', cloneRepo);
    it('file exists in repo', function () { expect(fs.existsSync(repodir + '/newfile')).to.be(true); });

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // check if the _first_ login via email succeeds
    it('can login via email', async function () { execSync(`cloudron install --location ${LOCATION} -p SSH_PORT=${SSH_PORT}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, email, password));

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // No SSO
    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION} -p SSH_PORT=${SSH_PORT}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login (no sso)', adminLogin);
    it('can logout', logout);

    it('uninstall app (no sso)', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION} -p SSH_PORT=${SSH_PORT}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can set avatar', setAvatar);
    it('can get avatar', checkAvatar);
    it('can add public key', addPublicKey);
    it('can create repo', createRepo);
    it('can clone the url', cloneRepo);
    it('can add and push a file', pushFile);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    xit('can admin login', adminLogin);
    xit('can send mail', sendMail);
    xit('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can get avatar', checkAvatar);
    it('can clone the url', cloneRepo);
    it('file exists in cloned repo', fileExists);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
